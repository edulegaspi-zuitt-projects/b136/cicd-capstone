const express = require("express");
const app = express();
const port = 4000;

app.get('/', (req, res) => {
  res.send('Hello World!')
})


app.get('/result', (req, res) => {
  res.send('Result is here.')
})


app.listen(process.env.PORT || port, () => {
	console.log(`API is now listening on port ${process.env.PORT || port}`)
});